class Board{
	private Square[][] tictactoeBoard;
	
	public Board(){ //Constructor
		this.tictactoeBoard = new Square[3][3];
		for(int i=0; i<tictactoeBoard.length;i++){
			for(int j=0; j<tictactoeBoard.length;j++){
				tictactoeBoard[i][j] = Square.BLANK;
			}
		}	
	}
	public String toString(){
		String board = "  ";
		for(int k = 0; k<tictactoeBoard.length;k++){ //Horizontal
			board += k+" ";
		}
		board += "\n";
		for(int i=0; i<tictactoeBoard.length;i++){ //Vertical
			board += i+" ";
			for(int j=0; j<tictactoeBoard.length;j++){
				board += tictactoeBoard[i][j] + " ";
			}
			board+="\n";
		}
		return board;
	}
	public boolean placeToken(int row, int col, Square playerToken){
		if(row <= this.tictactoeBoard.length && col <= this.tictactoeBoard.length){
			if(this.tictactoeBoard[row][col] == playerToken.BLANK){
				this.tictactoeBoard[row][col] = playerToken;
				return true;
			}else{
				return false;
			}
		}else{
			return false;
		}
	}
	public boolean checkIfFull(){
		for(int i=0; i<this.tictactoeBoard.length;i++){
			for(int k=0; k<this.tictactoeBoard.length;k++){
				if(this.tictactoeBoard[i][k] == Square.BLANK){
					return false;
				}
			}
		}
		return true;
	}
	private boolean checkIfWinningHorizontal(Square playerToken){
		boolean result = false;
		for(int i=0; i<this.tictactoeBoard.length;i++){
			for(int k=0, j=0; k<this.tictactoeBoard.length;k++){
				if(this.tictactoeBoard[i][k] == playerToken){
					j++;
				}
				if(j == this.tictactoeBoard.length){ 
				result = true;
			}
			}
		}
		return result;
	}
	private boolean checkIfWinningVertical(Square playerToken){
		boolean result = false;
		
		for(int m = 0; m<this.tictactoeBoard.length; m++){
			for(int i=0, j=0; i<this.tictactoeBoard.length;i++){
				if(this.tictactoeBoard[i][i] == playerToken){
						j++;
						break;
				}
				if(j==this.tictactoeBoard.length){
					result = true;
				}
			}
		}
		return result;
	}
	public boolean checkIfWinning(Square playerToken){
		boolean result = false;
		if(checkIfWinningHorizontal(playerToken)||checkIfWinningVertical(playerToken)){
			result = true;
		}
		return result;
	}
}
