import java.util.Scanner;
class TicTacToeGame{
	public static void main(String[] args){
		System.out.println("Welcome to the Tic Tac Toe Game!");
		System.out.println("Player 1's token: X\nPlayer 2's token: O");
		Scanner keyboard = new Scanner(System.in);
		Board board = new Board();
		boolean gameOver = false;
		int player = 1;
		Square playerToken = Square.X;
		while(!gameOver){ //loops a many turns until gameOver is true
			System.out.println(board.toString()); //prints board
			if(player == 1){ //choosing player token
				playerToken = Square.X;
			}else{
				playerToken = Square.O;
			}
			boolean tokenLoopResult = false; 
			while(!tokenLoopResult){ //this loop checks if the user's input is correct
				System.out.println("Player "+player+": it's your turn. Where do you want to place your token?");
				int row = keyboard.nextInt();
				int column = keyboard.nextInt();
				boolean decisionToken = board.placeToken(row,column,playerToken);
				if(!decisionToken){
					System.out.println("Put the correct input for column and row. \nOtherwise, there is already a token on the board.\nPlease try again!");
				}else{
					tokenLoopResult = true; //
				}
			}
			if(board.checkIfWinning(playerToken)){
				System.out.println("Player "+player+" is the winner!");
				gameOver = true;
			}
			else if(board.checkIfFull()){
				System.out.println("It's a tie!");
				gameOver = true;
			}
			else{
				if(player == 1){
					player++;
				}else{
					player--;
				}
			}
		}
	}
}